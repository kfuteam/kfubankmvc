﻿using System;
using System.Linq;
using System.Threading.Tasks;
using kfubankmvc.DbModels;
using kfubankmvc.Models;
using kfubankmvc.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace kfubankmvc.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private CardModel _cardModel;
        private BankContext db;

        public AccountController(UserManager<User> userManager, SignInManager<User> signInManager, BankContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _cardModel = new CardModel(context);
            db = context;
        }

        // GET
        public IActionResult Index()
        {
            Card defaultCard = _cardModel.getUserDefaultCard(_userManager.GetUserId(User));
            if (defaultCard.Number != null)
            {
                ViewBag.defaultCardNumber = defaultCard.Number;
                ViewBag.balance = _cardModel.GetCardBalance(defaultCard.Number);
                ViewBag.currency = _cardModel.getRuble().Name;
            }
            return View(ViewBag);
        }
        
        [HttpGet]
        public IActionResult CardNotExists()
        {
            return View();
        }

        [HttpGet]
        public IActionResult ShowCards()
        {
            Card2CardViewModel model = new Card2CardViewModel();
            model.CardsItems = _cardModel.getUserCards(_userManager.GetUserId(User))
                                        .Select(c => new SelectListItem { Value = c.Number, Text = c.Number })
                                        .ToList();
            return View(model);
        }

        [HttpGet]
        public IActionResult ShowVklads()
        {
            Card2VkladViewModel model = new Card2VkladViewModel();
            model.VkladsItem = _cardModel.getUserDeposit(_userManager.GetUserId(User))
                    .Select(c => new SelectListItem { Text = c.OwnerID })
                                        .ToList();
            return View(model);
        }



        [HttpGet]
        public IActionResult Card2Vklad()
        {
            Card2VkladViewModel model = new Card2VkladViewModel();
            model.CardsItems = _cardModel.getUserCards(_userManager.GetUserId(User))
                                        .Select(c => new SelectListItem { Value = c.Number, Text = c.Number })
                                        .ToList();
            model.VkladsItem = _cardModel.getUserDeposit(_userManager.GetUserId(User))
                    .Select(c => new SelectListItem { Value = c.Id.ToString(), Text = c.Id.ToString()})
                                        .ToList();
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Card2Vklad(Card2VkladViewModel model)
        {
            if (!_cardModel.IsUserCardOwner(_userManager.GetUserId(User), model.CardFrom))
            {
                return RedirectToAction("CardNotExists");
            }

            if (!_cardModel.IsCardExist(model.CardFrom))
            {
                return RedirectToAction("CardNotExists");
            }

            if (model.Sum > _cardModel.GetCardBalance(model.CardFrom))
            {
                return RedirectToAction("NotEnouthMoney");
            }

            Card card = _cardModel.getCardByNumber(model.CardFrom);
            Deposit deposit = _cardModel.getDepositById(model.DepositTo);
            
            await _cardModel.makeTransaction(model.Sum, card.checkingAccountId, deposit.AccountId);
            return RedirectToAction("SuccessTransaction");
        }



        [HttpGet]
        public IActionResult Card2Card()
        {
            Card2CardViewModel model = new Card2CardViewModel();
            model.CardsItems = _cardModel.getUserCards(_userManager.GetUserId(User))
                                        .Select(c => new SelectListItem { Value = c.Number, Text = c.Number })
                                        .ToList();
            return View(model);
        }
        
        [HttpPost]
        public async Task<IActionResult> Card2Card(Card2CardViewModel model)
        {
            if (!_cardModel.IsUserCardOwner(_userManager.GetUserId(User), model.CardFrom))
            {
                return RedirectToAction("CardNotExists");
            }

            if (!_cardModel.IsCardExist(model.CardFrom))
            {
                return RedirectToAction("CardNotExists");
            }

            if (model.Sum > _cardModel.GetCardBalance(model.CardFrom))
            {
                return RedirectToAction("NotEnouthMoney");
            }

            Card cardFrom = _cardModel.getCardByNumber(model.CardFrom);
            Card cardTo = _cardModel.getCardByNumber(model.CardTo);
            
            await _cardModel.makeTransaction(model.Sum, cardFrom.checkingAccountId, cardTo.checkingAccountId);
            return RedirectToAction("SuccessTransaction");
        }

        [HttpGet]
        [Authorize]
        public IActionResult TopUpMobile()
        {
            return View();
        }

        [HttpGet]
        [Authorize]
        public IActionResult NotEnouthMoney()
        {
            return View();
        }
        [HttpPost]
        [Authorize]
        public IActionResult TopUpMobile(TopUpMobileViewModel model)
        {
            var card = db.Cards.FirstOrDefault(u => u.Owner.Id == _userManager.GetUserId(User));
            if (card != null && card.Money > 0)
            {
                card.Money = card.Money - model.Amount;
                return View();
            }
            return RedirectToAction("NotEnouthMoney");
        }

        [HttpGet]
        [Authorize]
        public IActionResult RentPayment()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public IActionResult RentPayment(RentPaymentViewModel model)
        {
            var card = db.Cards.FirstOrDefault(u => u.Owner.Id == _userManager.GetUserId(User));
            if (card != null && card.Money > 0)
            {
                card.Money = card.Money - model.Amount;
                return View();
            }
            return RedirectToAction("NotEnouthMoney");
        }
        
        [HttpGet]
        public IActionResult SuccessTransaction()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Test()
        {
            var Companies = db.Companies.Where(c => c.Type.Id == 1);
            ViewBag.Companies = Companies;
            string card = Card.GetCreditCardNumbers(Card.MASTERCARD_PREFIX_LIST);
            ViewBag.card = card;
            return View();
        }
        [HttpGet]
        public ActionResult GetMyTransactions()
        {
            ViewBag.Transactions = _cardModel.getUserTransactions(_userManager.GetUserId(User));
            return View();
        }
        [HttpGet]
        public ActionResult Transaction(int id)
        {
            //db.Companies
            var CheckingAccount = db.CheckingAccounts.Find(id);
            ViewBag.account = CheckingAccount.Number;
            return View();
        }
        [HttpGet]
        public ActionResult ShowCompanies(int type)
        {
            ViewBag.Companies = _cardModel.getCompanies(type);
            /*var Comp = _cardModel.getCompanies(type)
                .Join(db.CheckingAccounts,
                c => c.checkingAccountId,
                ch => ch.Id,
                (c, ch) => new
                {
                    Id = c.Id,
                    Logo = c.Logo,
                    Name = c.Name,
                    TypeId = c.TypeId,
                    checkingAccount = ch.Number
                });*/
            //ViewBag.Companies = Companies;
            return View();
        }
    }
}