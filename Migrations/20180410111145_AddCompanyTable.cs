﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace kfubankmvc.Migrations
{
    public partial class AddCompanyTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Money",
                table: "Cards",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "OwnerId",
                table: "Cards",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "checkingAccountId",
                table: "Cards",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "currencyId",
                table: "Cards",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "CheckingAccount",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Number = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CheckingAccount", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Currency",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Currency", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Cards_OwnerId",
                table: "Cards",
                column: "OwnerId");

            migrationBuilder.CreateIndex(
                name: "IX_Cards_checkingAccountId",
                table: "Cards",
                column: "checkingAccountId");

            migrationBuilder.CreateIndex(
                name: "IX_Cards_currencyId",
                table: "Cards",
                column: "currencyId");

            migrationBuilder.AddForeignKey(
                name: "FK_Cards_AspNetUsers_OwnerId",
                table: "Cards",
                column: "OwnerId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Cards_CheckingAccount_checkingAccountId",
                table: "Cards",
                column: "checkingAccountId",
                principalTable: "CheckingAccount",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Cards_Currency_currencyId",
                table: "Cards",
                column: "currencyId",
                principalTable: "Currency",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cards_AspNetUsers_OwnerId",
                table: "Cards");

            migrationBuilder.DropForeignKey(
                name: "FK_Cards_CheckingAccount_checkingAccountId",
                table: "Cards");

            migrationBuilder.DropForeignKey(
                name: "FK_Cards_Currency_currencyId",
                table: "Cards");

            migrationBuilder.DropTable(
                name: "CheckingAccount");

            migrationBuilder.DropTable(
                name: "Currency");

            migrationBuilder.DropIndex(
                name: "IX_Cards_OwnerId",
                table: "Cards");

            migrationBuilder.DropIndex(
                name: "IX_Cards_checkingAccountId",
                table: "Cards");

            migrationBuilder.DropIndex(
                name: "IX_Cards_currencyId",
                table: "Cards");

            migrationBuilder.DropColumn(
                name: "Money",
                table: "Cards");

            migrationBuilder.DropColumn(
                name: "OwnerId",
                table: "Cards");

            migrationBuilder.DropColumn(
                name: "checkingAccountId",
                table: "Cards");

            migrationBuilder.DropColumn(
                name: "currencyId",
                table: "Cards");
        }
    }
}
