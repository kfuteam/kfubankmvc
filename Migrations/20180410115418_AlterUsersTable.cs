﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace kfubankmvc.Migrations
{
    public partial class AlterUsersTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cards_CheckingAccount_checkingAccountId",
                table: "Cards");

            migrationBuilder.DropForeignKey(
                name: "FK_Cards_Currency_currencyId",
                table: "Cards");

            migrationBuilder.DropForeignKey(
                name: "FK_Companies_CheckingAccount_checkingAccountId",
                table: "Companies");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Currency",
                table: "Currency");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CheckingAccount",
                table: "CheckingAccount");

            migrationBuilder.RenameTable(
                name: "Currency",
                newName: "Currencies");

            migrationBuilder.RenameTable(
                name: "CheckingAccount",
                newName: "CheckingAccounts");

            migrationBuilder.AddColumn<string>(
                name: "UserSurName",
                table: "AspNetUsers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserThirdName",
                table: "AspNetUsers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Currencies",
                table: "Currencies",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CheckingAccounts",
                table: "CheckingAccounts",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "Deposits",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AccountId = table.Column<int>(type: "int", nullable: true),
                    Amount = table.Column<int>(type: "int", nullable: false),
                    ExpireDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    OwnerId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    currencyId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Deposits", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Deposits_CheckingAccounts_AccountId",
                        column: x => x.AccountId,
                        principalTable: "CheckingAccounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Deposits_AspNetUsers_OwnerId",
                        column: x => x.OwnerId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Deposits_Currencies_currencyId",
                        column: x => x.currencyId,
                        principalTable: "Currencies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Transactions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Amount = table.Column<int>(type: "int", nullable: false),
                    accountFromId = table.Column<int>(type: "int", nullable: true),
                    accountToId = table.Column<int>(type: "int", nullable: true),
                    currencyId = table.Column<int>(type: "int", nullable: true),
                    date = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Transactions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Transactions_CheckingAccounts_accountFromId",
                        column: x => x.accountFromId,
                        principalTable: "CheckingAccounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Transactions_CheckingAccounts_accountToId",
                        column: x => x.accountToId,
                        principalTable: "CheckingAccounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Transactions_Currencies_currencyId",
                        column: x => x.currencyId,
                        principalTable: "Currencies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserCards",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    OwnerId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    userCardId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserCards", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserCards_AspNetUsers_OwnerId",
                        column: x => x.OwnerId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserCards_Cards_userCardId",
                        column: x => x.userCardId,
                        principalTable: "Cards",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Deposits_AccountId",
                table: "Deposits",
                column: "AccountId");

            migrationBuilder.CreateIndex(
                name: "IX_Deposits_OwnerId",
                table: "Deposits",
                column: "OwnerId");

            migrationBuilder.CreateIndex(
                name: "IX_Deposits_currencyId",
                table: "Deposits",
                column: "currencyId");

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_accountFromId",
                table: "Transactions",
                column: "accountFromId");

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_accountToId",
                table: "Transactions",
                column: "accountToId");

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_currencyId",
                table: "Transactions",
                column: "currencyId");

            migrationBuilder.CreateIndex(
                name: "IX_UserCards_OwnerId",
                table: "UserCards",
                column: "OwnerId");

            migrationBuilder.CreateIndex(
                name: "IX_UserCards_userCardId",
                table: "UserCards",
                column: "userCardId");

            migrationBuilder.AddForeignKey(
                name: "FK_Cards_CheckingAccounts_checkingAccountId",
                table: "Cards",
                column: "checkingAccountId",
                principalTable: "CheckingAccounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Cards_Currencies_currencyId",
                table: "Cards",
                column: "currencyId",
                principalTable: "Currencies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Companies_CheckingAccounts_checkingAccountId",
                table: "Companies",
                column: "checkingAccountId",
                principalTable: "CheckingAccounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cards_CheckingAccounts_checkingAccountId",
                table: "Cards");

            migrationBuilder.DropForeignKey(
                name: "FK_Cards_Currencies_currencyId",
                table: "Cards");

            migrationBuilder.DropForeignKey(
                name: "FK_Companies_CheckingAccounts_checkingAccountId",
                table: "Companies");

            migrationBuilder.DropTable(
                name: "Deposits");

            migrationBuilder.DropTable(
                name: "Transactions");

            migrationBuilder.DropTable(
                name: "UserCards");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Currencies",
                table: "Currencies");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CheckingAccounts",
                table: "CheckingAccounts");

            migrationBuilder.DropColumn(
                name: "UserSurName",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "UserThirdName",
                table: "AspNetUsers");

            migrationBuilder.RenameTable(
                name: "Currencies",
                newName: "Currency");

            migrationBuilder.RenameTable(
                name: "CheckingAccounts",
                newName: "CheckingAccount");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Currency",
                table: "Currency",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CheckingAccount",
                table: "CheckingAccount",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Cards_CheckingAccount_checkingAccountId",
                table: "Cards",
                column: "checkingAccountId",
                principalTable: "CheckingAccount",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Cards_Currency_currencyId",
                table: "Cards",
                column: "currencyId",
                principalTable: "Currency",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Companies_CheckingAccount_checkingAccountId",
                table: "Companies",
                column: "checkingAccountId",
                principalTable: "CheckingAccount",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
