﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using kfubankmvc.Models;

namespace kfubankmvc.DbModels
{
    public class CardModel
    {
        private BankContext db;

        public CardModel(BankContext context)
        {
            db = context;
        }
        
        public Card getCardByNumber(string number)
        {
            IQueryable<Card> cards = db.Cards;
            return cards.First(c => c.Number == number);
        }
        
        public List<Card> getUserCards(string userId)
        {
            IQueryable<Card> cards = db.Cards;
            return cards.Where(c => c.OwnerId == userId).ToList();
        }
        
        public Card getUserDefaultCard(string userId)
        {
            IQueryable<Card> cards = db.Cards;
            return cards.First(c => c.OwnerId == userId);
        }
        
        public Card getCardById(int cardId)
        {
            IQueryable<Card> cards = db.Cards;
            return cards.First(c => c.Id == cardId);
        }
        
        public bool IsUserCardOwner(string userId, string cardNumber)
        {
            IQueryable<Card> cards = db.Cards;
            return cards.Any(c => c.OwnerId == userId && c.Number == cardNumber);   
        }
        
        public bool IsCardExist(string cardNumber)
        {
            IQueryable<Card> cards = db.Cards;
            return cards.Any(c => c.Number == cardNumber);   
        }
        
        public bool IsAccountExist(int accountId)
        {
            IQueryable<CheckingAccount> accounts = db.CheckingAccounts;
            return accounts.Any(c => c.Id == accountId);   
        }

        public Deposit getDepositById(int number)
        {
            IQueryable<Deposit> deposits = db.Deposits;
            return deposits.First(c => c.Id == number);   
        }
        
        public CheckingAccount getAccountByNumber(string number)
        {
            IQueryable<CheckingAccount> accounts = db.CheckingAccounts;
            return accounts.First(c => c.Number == number);   
        }

        public int GetCardBalance(string cardNumber)
        {
            IQueryable<Card> cards = db.Cards;
            IQueryable<Transaction> transactions = db.Transactions;

            Card card = cards.First(c => c.Number == cardNumber);
            int plus = transactions
                .Where(t => t.accountToId == card.checkingAccountId)
                .Sum(t => t.Amount);
            
            int minus = transactions
                .Where(t => t.accountFromId == card.checkingAccountId)
                .Sum(t => t.Amount);

            return plus - minus;
        }

        public async Task makeTransaction(int sum, int checkingAccountFrom, int checkingAccountTo)
        {
            IQueryable<Card> cards = db.Cards;

            Transaction transaction = new Transaction();
            transaction.Amount = sum;
            transaction.accountFromId = checkingAccountFrom;
            transaction.accountToId = checkingAccountTo;
            transaction.currencyId = getRuble().Id;
            transaction.date = DateTime.Now;

            db.Transactions.Add(transaction);
            await db.SaveChangesAsync();
        }

        public Currency getRuble()
        {
            IQueryable<Currency> currencies = db.Currencies;
            return currencies.First(c => c.Id == 1);
        }
        public List<Transaction> getUserTransactions(string UserId)
        {
            List<Transaction> userTransactions = new List<Transaction>();
            foreach(var card in db.Cards.Where(t => t.OwnerId == UserId))
            {
                userTransactions.AddRange(db.Transactions.Where(t => t.accountFromId == card.checkingAccountId || t.accountToId == card.checkingAccountId));
            }
            return userTransactions;
        }

        public List<Deposit> getUserDeposit(string UserId)
        {
            var userDeposits = new List<Deposit>();
            userDeposits.AddRange(db.Deposits.Where(d => d.Owner.Id == UserId));
            return userDeposits;
        }
        public List<Company> getCompanies(int typeId)
        {
            var Companies = new List<Company>();
            Companies.AddRange(db.Companies.Where(c => c.Type.Id == typeId));
            return Companies;
        }
    }
}