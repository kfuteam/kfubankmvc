﻿using System;

namespace kfubankmvc.Models
{
    public class CompanyType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}