﻿using System.Web;

namespace kfubankmvc.Models
{
    public class UserCard
    {
        public int Id { get; set; }
        
        public int OwnerId { get; set; }
        public User Owner { get; set; }
        
        public int userCardId { get; set; }
        public Card userCard { get; set; }
    }
}