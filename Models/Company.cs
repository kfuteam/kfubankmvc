﻿using System.Web;
/*
 Type company
    1.Internet and TV
    2.Phone
    3.ГосКомпании
    
*/
namespace kfubankmvc.Models
{
    public class Company
    {
        public int Id { get; set; }
        
        public int checkingAccountId { get; set; }
        public CheckingAccount checkingAccount { get; set; }

        public string Logo { get; set; }
        public string Name { get; set; }
        
        public int TypeId { get; set;}
        public CompanyType Type { get; set;}

        public string getCheckingAccount()
        {
            return checkingAccount.Number;
        }
    }
}