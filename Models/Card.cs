﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace kfubankmvc.Models
{
    public class Card
    {
        public int Id { get; set; }
        public string Number{ get; set; }
        public DateTime ExpireDate{ get; set; }
        public string CheckCode{ get; set; }
        
        public User Owner { get; set; }
        public string OwnerId { get; set; }
        
        public CheckingAccount checkingAccount { get; set; }
        public int checkingAccountId { get; set; }
        
        public Currency currency { get; set; }
        public int currencyId { get; set; }

        public int Money { get; set; }
        
        public static bool IsValidate(string cardNumber)
        {
            if (cardNumber.Length != 16) return false;
            var sum = 0;
            var temp = 0;
            for(int i = 0; i < cardNumber.Length; i++)
            {
                if(i % 2 != 0)
                {
                    sum += Convert.ToInt32(cardNumber[cardNumber.Length - i - 1]);
                }
                else
                {
                    temp = Convert.ToInt32(cardNumber[cardNumber.Length - i - 1]) * 2;
                    if(temp > 9)
                    {
                        sum += ((temp / 10) + (temp % 10));
                    }
                    else
                    {
                        sum += temp;
                    }
                }
            }

            return (sum % 10 == 0);
        }
        public static string[] AMEX_PREFIX_LIST = new[] { "34", "37" };
        public static string[] MASTERCARD_PREFIX_LIST = new[]
                                                            {
                                                                "51",
                                                                "52", "53", "54", "55"
                                                            };
        public static string[] VISA_PREFIX_LIST = new[]
                                                    {
                                                        "4539",
                                                        "4556", "4916", "4532", "4929", "40240071", "4485", "4716", "4"
                                                    };

        private static string CreateFakeCreditCardNumber(string prefix, int length)
        {
            string ccnumber = prefix;

            while (ccnumber.Length < (length - 1))
            {
                double rnd = (new Random().NextDouble() * 1.0f - 0f);

                ccnumber += Math.Floor(rnd * 10);

                //sleep so we get a different seed

                Thread.Sleep(20);
            }


            // reverse number and convert to int
            var reversedCCnumberstring = ccnumber.ToCharArray().Reverse();

            var reversedCCnumberList = reversedCCnumberstring.Select(c => Convert.ToInt32(c.ToString()));

            // calculate sum

            int sum = 0;
            int pos = 0;
            int[] reversedCCnumber = reversedCCnumberList.ToArray();

            while (pos < length - 1)
            {
                int odd = reversedCCnumber[pos] * 2;

                if (odd > 9)
                    odd -= 9;

                sum += odd;

                if (pos != (length - 2))
                    sum += reversedCCnumber[pos + 1];

                pos += 2;
            }

            // calculate check digit
            int checkdigit =
                Convert.ToInt32((Math.Floor((decimal)sum / 10) + 1) * 10 - sum) % 10;

            ccnumber += checkdigit;

            return ccnumber;
        }


        public static string GetCreditCardNumbers(string[] prefixList)
        {
                int randomPrefix = new Random().Next(0, prefixList.Length - 1);

                if (randomPrefix > 1)
                {
                    randomPrefix--;
                }

                string ccnumber = prefixList[randomPrefix];

                return (CreateFakeCreditCardNumber(ccnumber, 16));
        }

    }
}   