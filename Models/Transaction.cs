﻿using System;
using System.Web;

namespace kfubankmvc.Models
{
    public class Transaction
    {
        public int Id { get; set; }
        
        public CheckingAccount accountFrom { get; set; }
        public int accountFromId { get; set; }

        public CheckingAccount accountTo { get; set; }
        public int accountToId { get; set; }

        public int Amount { get; set; }
        
        public Currency currency { get; set; }
        public int currencyId { get; set; }

        public DateTime date { get; set; }
    }
}