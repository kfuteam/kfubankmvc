﻿//Расчетный счет
using System.Web;

namespace kfubankmvc.Models
{
    public class CheckingAccount
    {
        public int Id { get; set; }
        public string Number{ get; set; }
    }
}