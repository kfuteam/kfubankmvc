﻿//Вклад
using System;
using System.Web;

namespace kfubankmvc.Models
{
    public class Deposit
    {
        public int Id { get; set; }
        
        public int AccountId { get; set; }
        public CheckingAccount Account { get; set; }
        
        public int Amount { get; set; }

        public int currencyId { get; set; }
        public Currency currency { get; set; }
        
        public DateTime ExpireDate { get; set; }

        public string OwnerID { get; set; }
        public User Owner { get; set; }
    }
}