﻿using Microsoft.AspNetCore.Identity;
 
namespace kfubankmvc.Models
{
    public class User : IdentityUser
    {
        public string UserSurName { get; set; }
        public string UserThirdName { get; set; }
    }
}