﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using kfubankmvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace kfubankmvc.ViewModels
{
    public class Card2VkladViewModel
    {
        [Required]
        [Display(Name = "Ваша карта")]
        public string CardFrom { get; set; }
         
        [Required]
        [Display(Name = "Вклад")]
        public int DepositTo { get; set; }
         
        [Display(Name = "Сумма")]
        public int Sum { get; set; }

      
        public List<SelectListItem> CardsItems{ get; set; }
        public List<SelectListItem> VkladsItem { get; set; }


    }
}