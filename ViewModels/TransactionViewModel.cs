﻿using System.ComponentModel.DataAnnotations;

namespace kfubankmvc.ViewModels
{
    public class TransactionViewModel
    {
        [Required]
        [Display(Name = "Куда отправляем?")]
        public string AccountTo { get; set; }
         
        [Display(Name = "Сумма")]
        public int Sum { get; set; }
    }
}