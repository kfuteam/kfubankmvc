﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using kfubankmvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace kfubankmvc.ViewModels
{
    public class Card2CardViewModel
    {
        [Required]
        [Display(Name = "Ваша карта")]
        public string CardFrom { get; set; }
         
        [Required]
        [Display(Name = "Куда отправляем?")]
        public string CardTo { get; set; }
         
        [Display(Name = "Сумма")]
        public int Sum { get; set; }

//        public string UserId { get; set; }

//        public List<Card> Cards { get; set; }

//        public Card2CardViewModel()
//        {
//            CardsItems = CardsListToSelectListItems(Cards);
//        }

        public List<SelectListItem> CardsItems { get; set; }

//        private static List<SelectListItem> CardsListToSelectListItems(List<Card> cards)
//        {
//            List<SelectListItem> items = new List<SelectListItem>();
////            items.Add(new SelectListItem{ Value = c.Number, Text = c.Number });
//            cards.ForEach(c => { Console.WriteLine(c.Number); });
//
//            return items;
//        }
    }
}