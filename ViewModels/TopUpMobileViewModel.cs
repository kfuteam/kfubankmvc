﻿using kfubankmvc.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace kfubankmvc.ViewModels
{
    public class TopUpMobileViewModel
    {
        [Required]
        [Display(Name = "PhoneNumber")]
        public int PhoneNumber { get; set; }
        [Required]
        [Display(Name = "Amount")]
        public int Amount { get; set; }
    }
}
