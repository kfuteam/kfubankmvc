﻿using kfubankmvc.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace kfubankmvc.ViewModels
{
    public class RentPaymentViewModel
    {
        [Required]
        [Display(Name = "Лицевой счет")]
        public int RentNumber { get; set; }
        [Required]
        [Display(Name = "Сумма")]
        public int Amount { get; set; }
    }
}
